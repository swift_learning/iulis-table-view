//
//  ViewController.swift
//  iulis-table-view
//
//  Created by iulian david on 11/9/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,
    UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var uglyThings = ["https://s-media-cache-ak0.pinimg.com/736x/f1/9a/51/f19a5199180cc1f5c82bb5367fca65b8.jpg",
                      "https://www.newincite.com/wp-content/uploads/2014/12/Ugly-Baby.jpg",
                      "https://i.imgur.com/NFADDBr.jpg",
                      "https://webstore.ugly-things.com/images/default/240%20UT%20black%20t%20shirt.JPG",
                      "https://65.media.tumblr.com/tumblr_lcs3tv5zws1qf6hkxo1_400.jpg"]
    var uglyTitles = ["Man this is ugly", "This one ins't so bad", "Boo hoo hoo", "Wooo man. No Thanks!",
                      "Somebody turn off the screen"]
    
    var TableData = [(UIImage, String)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        get_data_from_urls()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func get_data_from_urls(){
        for index in 0..<uglyThings.count {
            get_data_from_url(uglyThings[index],uglyTitles[index])
        }
    }
    
    func get_data_from_url(_ link: String,_ title: String){
        
        let url:URL = URL(string: link)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) in
            //tratare erori pe async
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                 self.load_failover_image(title)
                return
            }
            
            self.load_image(data, text: title)
            
            
        })
        
        task.resume()
    }
    
    func load_failover_image(_ text:String) {
        
        TableData.append((UIImage(named: "Ugly")!, text))
        DispatchQueue.main.async(execute: {self.do_table_refresh()})

    }
    
    func load_image(_ data: Data?, text: String)
    {
        
        
        var image: UIImage!
        
        
        if  data != nil {
            image = UIImage(data: data!)
            
        } else {
                image = UIImage(named: "Ugly")
                
            }
       
        TableData.append((image, text))
        
        
        DispatchQueue.main.async(execute: {self.do_table_refresh()})
        
    }
    
    func do_table_refresh()
    {
        self.tableView.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "uglyCell") as? UglyCell{
            
            
            cell.configureCell(image: TableData[indexPath.row].0, text: TableData[indexPath.row].1)
            
            return cell
            
        } else {
                return UglyCell()
        }
 
     }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableData.count
    }

}

